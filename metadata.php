<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id'          => 'dre_quickorder',
    'title'       => [
        'en' => '<img src="../modules/bender/dre_quickorder/out/img/favicon.ico" title="Bodynova Quick Order Modul">odynova Quick Order',
        'de' => '<img src="../modules/bender/dre_quickorder/out/img/favicon.ico" title="Bodynova Quick Order Modul">odynova Quick Order',
    ],
    'description' => [
        'en' => 'Modul for making orders even quicker',
        'de' => 'Modul zur Schnellbestellung',
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '0.1',
    'author'      => 'Bodynova GmbH',
    'url'         => 'https://sales.bodynova.com',
    'email'       => 'support@bodynova.de',
    'controllers'       => [
        'quickorder'            => \Bender\dre_QuickOrder\Application\Controller\quickorder::class,
        'dre_ajax_quickorder'   => \Bender\dre_QuickOrder\Application\Controller\dre_ajax_quickorder::class,
        'adressverwaltung'      => \Bender\dre_QuickOrder\Application\Controller\adressverwaltung::class
    ],
    'extend'      => [
        OxidEsales\Eshop\Application\Controller\ContactController::class =>  Bender\dre_QuickOrder\Application\Controller\dre_ContactController::class
        //Bender\dre_QuickOrder\Application\Model\dre_Address::class => OxidEsales\Eshop\Application\Model\Address::class
    ],
    'templates'   => [
        'dre_quickorder_form.tpl'         => 'bender/dre_quickorder/Application/views/tpl/dre_quickorder_form.tpl',
        'bn_quickorder_form.tpl'         => 'bender/dre_quickorder/Application/views/tpl/bn_quickorder_form.tpl',
        'dre_quick_json.tpl'              => 'bender/dre_quickorder/Application/views/tpl/dre_quick_json.tpl',
        'dre_adressverwaltung.tpl'        => 'bender/dre_quickorder/Application/views/tpl/dre_adressverwaltung.tpl',
        'bn_adressverwaltung.tpl'        => 'bender/dre_quickorder/Application/views/tpl/bn_adressverwaltung.tpl',
        'dre_services.tpl'        => 'bender/dre_quickorder/Application/views/tpl/widget/footer/dre_services.tpl',
        'bn_services.tpl'        => 'bender/dre_quickorder/Application/views/tpl/widget/footer/bn_services.tpl',
    ],
    'blocks'      => array(
        array(
            'template' => 'page/account/dashboard.tpl',
            'block' => 'account_dashboard_col1',
            'file' => 'Application/views/blocks/page/account/dre_dashboard.tpl'
        ),
        array(
            'template' => 'widget/footer/services.tpl',
            'block' => 'footer_services',
            'file' => 'Application/views/blocks/widget/footer/dre_services.tpl'
        ),
    ),
    'settings'    => [
        /*[
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_orders',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_payments',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_topsellerCategories',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_ordersStorno',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_ordersState',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_orderValues',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_customerAcountTypes',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_customerBought',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_customerNewsletters',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_articleInfos',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_articleTopActive',
            'type'  => 'bool',
            'value' => '0'
        ],
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_articleTopAll',
            'type'  => 'bool',
            'value' => '0'
        ],
        */
    ],
];
