var objSearchTimeout = null;

$('#OXARTNUM').keyup(function() {

    if(objSearchTimeout)
        window.clearInterval(objSearchTimeout);
    objSearchTimeout = window.setTimeout(lookupForItems, 500);
})


function lookupForItems() {
    //
	var loged = $('#OXARTNUM').val();
	console.log(loged);
	
    var res = $.ajax({
        type: "GET",
        dataType: "json",
        url: '/index.php?cl=quickorder&fnc=getArticles&artnum=' + $('#OXARTNUM').val(),
        async : false,
        success : function(res) {
            //
            console.log(res);
            $('#newEntryAutoComplete').html('');
            //
            if(res.articles!=undefined)
            {
                    $.each(res.articles, function(key,val) {
                        if(this.bnflagbestand !== "2"){
                            $('#newEntryAutoComplete').append(
                                '<div style="border-bottom:1px solid #ccc !important" class="row" onclick="addQuickorderItem(\'' + addslashes(JSON.stringify(val)) + '\');">'+
                                '                                <div class="col-sm-1" style="white-space: nowrap">'+this.artnum+'</div>' +
                                '                                <div class="col-sm-6" >'+this.title+'</div>' +
                                '                                <div class="col-sm-1" style="white-space: nowrap">'+this.price+'</div>' +
                                '                                <div class="col-sm-1" style="white-space: nowrap;height:20px"></div>' +
                                '                                <div class="col-sm-2 stock" id="QuickPositions"><div class="stock' + this.bnflagbestand + '"></div>' +
                                '</div>'
                            );
                        } else {
                            $('#newEntryAutoComplete').append(
                                '<div style="border-bottom:1px solid #ccc !important" class="row" onclick="alertify.error(\'Nicht kaufbar!\');">'+
                                '                                <div class="col-sm-1" style="white-space: nowrap">'+this.artnum+'</div>' +
                                '                                <div class="col-sm-6" >'+this.title+'</div>' +
                                '                                <div class="col-sm-1" style="white-space: nowrap">'+this.price+'</div>' +
                                '                                <div class="col-sm-1" style="white-space: nowrap;height:20px"></div>' +
                                '                                <div class="col-sm-2 stock" id="QuickPositions"><div class="stock' + this.bnflagbestand + '"></div>' +
                                '</div>'
                            );
                        }

                    });
            }
            else if(res.suggestions!=undefined)
            {
                var strSuggestion = strSuggestionHtml = '';
                //
                $.each(res.suggestions, function(key, val) {
                    strSuggestion+= val.word + ' ';
                    strSuggestionHtml+=(val.changed ? '<b><i>' : '') + val.word + (val.changed ? '</i></b>' : '') + ' ';
                });
                //
                $('#newEntryAutoComplete').append(
                    res.suggesttext
                    +
                    '<a href="#" onclick="$(\'#OXARTNUM\').val(\'' + strSuggestion +'\');lookupForItems();">' + strSuggestionHtml + '</a>'
                );
            }
        },
        error: function(res){
            console.log(res + 'err');
        }
    });
}

/**
 * Funktion zur Löschung eines Items aus Warenkorb in Quickorder
 */

function deleteBasketItem(x){
    $('#1'+x+'').remove();
    $('#2'+x+'').remove();
    $('#3'+x+'').remove();
    $('#4'+x+'').remove();
    $('#5'+x+'').remove();
    //$('#6'+x+'').remove();
}


/**
 *
 * @param params
 */
function addQuickorderItem(params) {
    //
    eval('params = ' + params);
    // clear result list
    $('#newEntryAutoComplete').html('');
    // clear inputs
    $('#OXARTNUM').val('');
    $('#OXTITLE').val('');
    // add row to table
    var strEntry =
        '   <div id="1'+params.artnum+'" class="col-sm-2" style="white-space: nowrap;border-bottom:1px solid #ccc !important">' + params.artnum + '</div>' +
        '   <div id="2'+params.artnum+'" class="col-sm-6 title" style="border-bottom:1px solid #ccc !important">' + params.title + '</div>' +
        '   <div id="3'+params.artnum+'" class="col-sm-1 price" style="white-space: nowrap;border-bottom:1px solid #ccc !important">' + params.price + '</div>' +
        '   <div id="4'+params.artnum+'" class="col-sm-1 amount" style="white-space: nowrap; border-bottom:1px solid #ccc !important"><input style="width:40px;" type="text" name="amount[' + params.oxid + ']" value="1"></div>' +
        '   <div id="5'+params.artnum+'" class="col-sm-2 stock" id="QuickPositions" style="border-bottom:1px solid #ccc !important"><div class="stock' + params.bnflagbestand + '"></div><button type="button" class="btn btn-xs btn-outline-danger pull-right" onclick="deleteBasketItem(\''+params.artnum+'\')"><span class="fa fa-trash"></span></button></div>'
        //'   <div id="6'+params.artnum+'" class="col-sm-1" style="border-bottom:1px solid #ccc !important"><button type="button" class="btn btn-xs btn-outline-danger" onclick="deleteBasketItem(\''+params.artnum+'\')"><span class="fa fa-trash"></span></button></div>'
        ;
    //
    $(strEntry).insertBefore('#QuickPositions #newEntry');
    // ende
}

/**
 *
 */
function placeQuickOrder(e1, e2, e3) {
    // disable button with ladda
    //var ladda = Ladda.create(document.querySelector('#btnPlaceOrder'));
    //var spinner= '  <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>';
    //$('#btnPlaceOrder').append(spinner);
    $('#spinnerOrder').attr('style','display:block');

    //ladda.start();
    $("#frmQuickOrder").submit(
            $.ajax({
                url : '/index.php?cl=quickorder&fnc=submitBasket',
                type: 'POST',
                data: $("#frmQuickOrder").serialize(),
                dataType : 'json',
                success: function(res){
                    $('#spinnerOrder').attr('style','display:none');
                    console.log(res);
                    //ladda.stop();
                    // close preview modal and open confirmation when successful
                    // console.log(res);
                    if(res.state == 1) {
                        $('#previeworder').modal('hide');


                        // console.log(res);
                        // var strsuccessMsg = res.msg;
                        // var ordernr = res.ordernr;
                        // var cont = formatString(strsuccessMsg , ordernr);

                        $('#ordercomplete .modal-body').append('<div class="alert alert-success" role="alert">#' + res.ordernr  + '<div/>');
                        $('#ordercomplete').modal({});
                    } else {
                        //
                        var strErrorMsg = '';
                        switch(res.state) {
                            case 0:
                                strErrorMsg = e1;
                                break;
                            case -1:
                                strErrorMsg = e2;
                                break;
                        }
                        //
                        $('#previeworder .modal-body').prepend('<div class="alert alert-danger" role="alert">' + strErrorMsg + '</div>');
                        // ende
                    }
                },
                error:function(res){
                    $('#spinnerOrder').attr('style','display:none');

                    alertify.error(e3);

                    console.log(res);
                }
            })

    );


    //
    /*
    $('#frmQuickOrder').ajaxForm({
        type: "POST",
        dataType: 'json',
        url: '/index.php?cl=quickorder&fnc=submitBasket',
        success:function(res) {
            //
            ladda.stop();
            // close preview modal and open confirmation when successful
	        // console.log(res);
            if(res.state == 1) {
                $('#previeworder').modal('hide');
                
	
                // console.log(res);
	            // var strsuccessMsg = res.msg;
	            // var ordernr = res.ordernr;
	            // var cont = formatString(strsuccessMsg , ordernr);
	            
	            $('#ordercomplete .modal-body').append('<div class="alert alert-success" role="alert">#' + res.ordernr  + '<div/>');
	            $('#ordercomplete').modal({});
            } else {
                //
                var strErrorMsg = '';
                switch(res.state) {
                    case 0:
                        strErrorMsg = e1;
                    break;
                    case -1:
                        strErrorMsg = e2;
                    break;
                }
                //
                $('#previeworder .modal-body').prepend('<div class="alert alert-danger" role="alert">' + strErrorMsg + '</div>');
                // ende
            }
            // ende
        },
        error:function(res){
            alertify.error(e3);
            
            console.log(res);
        }
    }).submit(); */
    // ende
}





//
//
//
//
//

/**
 *
 */
function setAdress(oxid, company, sal, fname, lname, street, streetnr, addinfo, zip, city, countryid, fon, message = null ) {
    //
    if($('#iswhitelabel').attr('disabled')){
        $('#iswhitelabel').attr('disabled',false);
    } else {
        $('#iswhitelabel').attr('disabled',true);
    }
    $('#addressbookid').val(oxid);
    $('#oxcompany').val(company).attr('placeholder','');
    $('#oxsal').val(sal);
    $('#oxfname').val(fname).attr('placeholder','');
    $('#oxlname').val(lname).attr('placeholder','');
    $('#oxstreet').val(street).attr('placeholder','');
    $('#oxstreetnr').val(streetnr).attr('placeholder','');
    $('#oxaddinfo').val(addinfo).attr('placeholder','');
    $('#oxzip').val(zip).attr('placeholder','');
    $('#oxcity').val(city).attr('placeholder','');
    $('#oxfon').val(fon).attr('placeholder','');
    $('#oxcountryid').val(countryid).attr('placeholder','');
    //
    $('#adressBookAutocomplete').html('');
    $('#searchadressbook').val('');
    // ende
    if(message !== null){
        alertify.success(message);
    }

}


/**
 *
 */
function searchInAdressbook() {
    //
    var res = $.ajax({
        type: "GET",
        dataType: "json",
        url: '/index.php?cl=dre_ajax_quickorder&fnc=getAdressAutocomplete&search=' + $('#searchadressbook').val(),
        async : false,
        success:function (response) {
            console.log('success');
            console.log(response);

        }
    }).responseJSON;

    $('#adressBookAutocomplete').html('');
    //
    $.each(res, function() {
        //
        $('#adressBookAutocomplete').append(
            '<div onclick="setAdress(\'' + this.oxid + '\',\'' + this.company + '\',\'' + this.sal + '\',\'' + this.fname + '\',\'' + this.lname + '\',\'' + this.street + '\',\'' + this.streetnr + '\',\'' + this.addinfo + '\',\'' + this.zip + '\',\'' + this.city + '\',\'' + this.countryid + '\',\'' + this.fon + '\',\'' + this.message + '\')" class="addressBookItem">' +
                (this.company!='' ? this.company + '<br>': '') +
                this.fname + ' ' + this.lname + '<br>' +
                this.street + ' ' + this.streetnr + '<br>' +
                this.zip + ' ' + this.city + '<br>' +
                this.addinfo  +
            '</div>'
        );
        // ende
    });
     // ende
}

var timeOutHolderAdressbookSearch = null;

$(document).ready(function() {
    $('#searchadressbook').keyup(function() {
        //
        if(timeOutHolderAdressbookSearch)
            window.clearTimeout(timeOutHolderAdressbookSearch);
        //
        timeOutHolderAdressbookSearch = window.setTimeout('searchInAdressbook()', 500);
        // ende
    });
    //
    $('#addressbookid, #oxcompany, #oxsal, #oxfname, #oxlname, #oxstreet, #oxstreetnr, #oxaddinfo, #oxzip, #oxcity, #oxcountryid').change(function() {
        $('#addressbookid').val('');
    });
    //
    /*
    $('#searchadressbook').val('geil').keyup();
    addQuickorderItem('{\'oxid\':\'edae7ac1f920327673822d2c2bc1f404\',\'artnum\':\'hms1\',\'title\':\'Hamam Set I zum Sonderpreis\',\'price\':4.96,\'bnflagbestand\':\'0\'}');
    addQuickorderItem('{\'oxid\':\'eda3ce1925f2b48e3947eadc87bcb196\',\'artnum\':\'hms2b\',\'title\':\'Hamam Set II blau, zum Sonderpreis\',\'price\':10,\'bnflagbestand\':\'0\'}');
    addQuickorderItem('{\'oxid\':\'8894ac5e442372d96.61335702\',\'artnum\':\'ms88b\',\'title\':\'Meditations Set: Zafu & Zabuton(80x80 cm) | blau\',\'price\':62.94,\'bnflagbestand\':\'0\'}');
    */
    // ende
});

function showConfirmQuickOrder(count, article, price, sum) {
    var strOverview = "<table id=\"orderPreview\"><tr class=\"headline\">";
    strOverview += "<td class=\"title\">" + article + "</td>";
    strOverview += "<td class=\"price\">" + price + "</td>";
    strOverview += "<td class=\"amount text-right\">" + count + "</td></tr>";
    var summe = 0.00;
    var quickposi = $('#QuickPositions')[0].children;
    for(var i = 5;i<quickposi.length-4;i++){
        if((i % 5) === 1){
            strOverview += '<tr>';
            strOverview += '<td class="title">' + quickposi[i].innerHTML + '</td>';
        }
        if((i % 5) === 2){
            strOverview += '<td class="price">' + quickposi[i].innerHTML + '</td>';
            var einzelpreis = quickposi[i].innerHTML;
        }
        if((i % 5) === 3){
            console.log(quickposi[i]);
            strOverview += '<td class="amount text-right">' + quickposi[i].firstChild.value + '</td></tr>';
            var amount = quickposi[i].firstChild.value;
            console.log('einzelpreis' + einzelpreis + '  ,  amount:  ' + amount);
            summe += parseFloat(einzelpreis.replace(',','.'))*parseInt(amount);
        }

    }
    console.log(summe);

    $(".item").each(function () {
        strOverview += "<tr>";
        strOverview += "  <td class=\"amount\">" + $(this).children(".amount").children("input").val() + "</td>";
        strOverview += "  <td class=\"title\">" + $(this).children(".title").text() + "</td>";
        strOverview += "  <td class=\"price\">" + $(this).children(".price").text() + "</td>";
        strOverview += "</tr>";
        summe = summe + (parseFloat($(this).children(".price").text().replace(',','.'))*(parseInt($(this).children(".amount").children("input").val())));
    });
    strOverview +="<tr><td></td><td style='text-align: right'><b>" + sum + " " + summe.toFixed(2).replace('.', ',') + "€</b></td><td></td></tr>";

    strOverview += "</table>";
    $("#previeworder .modal-body").html(strOverview);
    $("#previeworder").modal();
}