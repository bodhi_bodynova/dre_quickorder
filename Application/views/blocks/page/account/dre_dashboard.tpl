<ul class="list-group" style="margin-bottom: 2px;">
	<li class="list-group-item">
		<a id="linkAccountPassword"
		   href="[{oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:" cl=account_password"}]"
		rel="nofollow">
		[{oxmultilang ident="CHANGE_PASSWORD"}]
		</a>
	</li>
	<li class="list-group-item">
		<a id="linkAccountNewsletter"
		   href="[{oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:" cl=account_newsletter"}]"
		rel="nofollow">
		[{oxmultilang ident="NEWSLETTER_SETTINGS"}]
		</a><br>
		[{oxmultilang ident="NEWSLETTER_SUBSCRIBE_CANCEL"}]
	</li>
	<li class="list-group-item">
		<a id="linkAccountBillship"
		   href="[{oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:" cl=account_user"}]" rel="nofollow">
		[{oxmultilang ident="BILLING_SHIPPING_SETTINGS"}]
		</a><br>
		[{oxmultilang ident="UPDATE_YOUR_BILLING_SHIPPING_SETTINGS"}]
	</li>
	<li class="list-group-item">
		<a id="linkAccountOrder"
		   href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:" cl=account_order"}]" rel="nofollow">
		[{oxmultilang ident="ORDER_HISTORY"}]
		</a><br>
		[{oxmultilang ident="ORDERS" suffix="COLON"}] [{$oView->getOrderCnt()}]
	</li>
	<li class="list-group-item">
		<a id="linkMyShoppingLists"
		   href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:" cl=myfavorites&fnc=showLists"}]"
		rel="nofollow">
		[{oxmultilang ident="BTN_MYFAVORITES_LISTS"}]
		</a><br>
		[{oxmultilang ident="MYFAVORITES_LISTS_DESC"}]
	</li>
	[{* Quickorder *}]
	<li class="list-group-item">
		<a id="linkQuickorder"
		   href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:" cl=quickorder&fnc=showForm"}]"
		rel="nofollow">
		[{oxmultilang ident="BTN_QUICKORDER"}]
		</a><br>
		[{oxmultilang ident="QUICKORDER_DESC"}]
	</li>
