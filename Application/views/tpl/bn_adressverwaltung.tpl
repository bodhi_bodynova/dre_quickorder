[{capture append="oxidBlock_content"}]
    <div class="container-fluid">
        [{oxscript include=$oViewConf->getModuleUrl('dre_quickorder','out/src/dre_adressverwaltung.js') priority=1}]

        [{*if $oView->getClassName() eq "adressverwaltung" && !$blHideBreadcrumb}]
			[{include file="widget/breadcrumb.tpl"}]
		[{/if*}]

        [{assign var="userid" value=$oxcmp_user->oxuser__oxid->value}]
        [{*$oxcmp_user->oxuser__oxid->value|var_dump*}]
        [{assign var="adressen" value=$oView->allAdress($userid)}]
        <div class="card" style="margin-bottom:180px;height:auto;">
            <div class="card-header" role="tab">
                <h4 class="pageHead"><strong>[{oxmultilang ident="BTN_ADRESSVERWALTUNG"}]</strong></h4>
            </div>

            [{*<form action="[{$oViewConf->getCurrentHomeDir()}]" method="post">
                <input type="hidden" name="cl" value="account_user">
                <button class="btn btn-outline-info" type="submit" style="margin-top:10px;margin-left:5px !important;"><i class="fa fa-pencil"></i>&nbsp;[{ oxmultilang ident="CHANGE" }]</button>
            </form>*}]

            <div class="card-body">
                <div class="row">
                    [{foreach from=$adressen item="adresse"}]
                    <div class="adressverwaltunggroß" style="border:1px solid rgba(0,0,0,.1);padding:5px">
                        <button class="btn btn-outline-danger pull-right" onclick="delAdress('[{$adresse.OXID}]');"><span class="fa fa-trash"></span></button>
                        [{$adresse.OXCOMPANY}]<br>
                        <b>[{$adresse.OXFNAME}] [{$adresse.OXLNAME}]</b><br>
                        [{$adresse.OXSTREET}]  [{$adresse.OXSTREETNR}]<br>
                        [{$adresse.OXZIP}] [{$adresse.OXCITY}]<br>
                        [{$adresse.OXCOUNTRY}]<br>
                        [{if $adresse.OXFON}]<i class="fa fa-phone"></i>[{/if}][{$adresse.OXFON}]
                        <button class="btn btn-outline-success pull-right bntooltip" data-toggle="tooltip" title="[{oxmultilang ident='CHANGE'}]" style="margin-top:-15px"
                                onclick="changeAddress('[{$adresse.OXID|escape:'html'}]',
                                        '[{$adresse.OXCOMPANY|escape:'html'}]',
                                        '[{$adresse.OXFNAME|escape:'html'}]',
                                        '[{$adresse.OXLNAME|escape:'html'}]',
                                        '[{$adresse.OXSTREET|escape:'html'}]',
                                        '[{$adresse.OXSTREETNR|escape:'html'}]',
                                        '[{$adresse.OXZIP|escape:'html'}]',
                                        '[{$adresse.OXCITY|escape:'html'}]',
                                        '[{$adresse.OXCOUNTRY|escape:'html'}]',
                                        '[{$adresse.OXCOUNTRYID|escape:'html'}]',
                                        '[{$adresse.OXFON|escape:'html'}]',
                                        '[{$adresse.OXADDINFO|escape:'html'}]')"
                        ><i class="fa fa-pencil" ></i></button>
                    </div>
                    [{/foreach}]
                </div>

                [{*$adresse|var_dump*}]
            </div>

            [{*$adresse|var_dump*}]
        </div>
    </div>


    [{/capture}]
[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1 }]
<div id="addressModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="[{$oViewConf->getCurrentHomeDir()}]" method="post">
            <div class="modal-header">
                <h5 class="modal-title">[{oxmultilang ident="BTN_ADRESSVERWALTUNG"}]</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <input type="hidden" name="cl" value="adressverwaltung">
                    <input type="hidden" name="fnc" value="changeAddress">
                    <input type="hidden" name="oxid" value="">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="width: 150px;">[{oxmultilang ident="COMPANY"}]</span>
                        </div>
                        <input name="company" type="text" class="form-control">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="width: 150px;">[{oxmultilang ident="FIRST_NAME"}]</span>
                        </div>
                        <input name="fname" type="text" class="form-control">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="width: 150px;">[{oxmultilang ident="LAST_NAME"}]</span>
                        </div>
                        <input name="lname" type="text" class="form-control">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="width: 150px;">[{oxmultilang ident="STREET"}]</span>
                        </div>
                        <input name="street" type="text" class="form-control">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="width: 150px;">[{oxmultilang ident="STREETNO"}]</span>
                        </div>
                        <input name="nr" type="text" class="form-control" >
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="width: 150px;">[{oxmultilang ident="POSTAL_CODE"}]</span>
                        </div>
                        <input name="zip" type="text" class="form-control">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="width: 150px;">[{oxmultilang ident="POSTAL_CITY"}]</span>
                        </div>
                        <input name="city" type="text" class="form-control">
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="width: 150px;">[{oxmultilang ident="COUNTRY"}]</span>
                        </div>
                        [{*<input disabled name="country" type="text" class="form-control">*}]
                        <select name="country" class="custom-select">
                            [{assign var="langId" value=$oViewConf->getActLanguageId()}]
                            [{assign var="countries" value=$oView->getCountries($langId)}]
                            [{if $langId == 0}]
                                [{assign var="lang" value="OXTITLE"}]
                            [{elseif $langId == 1}]
                                [{assign var="lang" value="OXTITLE_1"}]
                            [{elseif $langId == 2}]
                                [{assign var="lang" value="OXTITLE_2"}]
                            [{/if}]
                            [{foreach from=$countries item="country"}]
                                <option value="[{$country.OXID}]#[{$country.$lang}]">[{$country.$lang}]</option>
                            [{/foreach}]
                        </select>
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="width: 150px;">[{oxmultilang ident="PHONE"}]</span>
                        </div>
                        <input name="fon" type="text" class="form-control">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">

                            <span class="input-group-text" style="width: 150px;">[{oxmultilang ident="ADDITIONAL_INFO"}]</span>
                        </div>
                        <input name="info" type="text" class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info" style="left: 10px;margin-right: 20px;" data-dismiss="modal">[{oxmultilang ident="CancelAddressChange"}]</button>
                <button type="submit" class="btn btn-outline-success">[{oxmultilang ident="SAVE"}]</button>
            </div>
            </form>
        </div>
    </div>
</div>