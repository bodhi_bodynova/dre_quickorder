[{capture append="oxidBlock_content"}]
    <div class="container">
        [{assign var="dropship" value=$oxcmp_user->oxuser__dropship->value}]
        [{assign var="url" value=$oViewConf->getModuleUrl('dre_quickorder')|cat:"out/src/bnchild_bodynova-quickorder.js"}]
        [{oxscript include=$url priority=10}]
        <div class="card" style="margin-bottom:140px; height:auto;">
            <div class="card-header">
                <h1 class="card-title">[{*$oView->geTitle()*}]<b>[{oxmultilang ident="BTN_QUICKORDER"}]</b></h1>
                <div class="pull-right" style="margin-top: -27px;">
                    [{if $oView->getClassName() eq "user" && !$blHideBreadcrumb}]
                    [{include file="widget/breadcrumb.tpl"}]
                    [{/if}]
                </div>
            </div>
            <div class="card-body">

                [{if $dropship == 1}]


                <div class="form-horizontal">
                    <form id="frmQuickOrder" method="POST" action="/index.php?cl=quickorder&fnc=addToBasket" role="form"
                          enctype="multipart/form-data">
                        <div class="form-group row" id="QuickPositions">
                                [{*<div class="col-md-2 d-none d-md-block" style="white-space: nowrap;font-weight:bold; overflow:hidden !important;">[{oxmultilang ident="QUICKORDER_ARTNUM"}]</div>
                                <div class="col-md-6 d-none d-md-block" style="white-space: nowrap;font-weight:bold; overflow:hidden !important;">[{oxmultilang ident="QUICKORDER_ARTICLE"}]</div>
                                <div class="col-md-1 d-none d-md-block" style="padding-left:0 !important;white-space: nowrap;font-weight:bold; overflow:hidden !important;">[{oxmultilang ident="QUICKORDER_SINGLE_PRICE"}]</div>
                                <div class="col-md-1 d-none d-md-block" style="white-space: nowrap;font-weight:bold; overflow:hidden !important;">[{oxmultilang ident="QUICKORDER_COUNT"}]</div>
                                <div class="col-md-2 d-none d-md-block" style="white-space: nowrap;font-weight:bold; overflow:hidden !important;">[{oxmultilang ident="QUICKORDER_VERFUEGBAR_DELETE"}]</div>
                                *}]
                                <div class="col-12" id="newEntry">
                                    <input type="text" id="OXARTNUM" name="OXARTNUM"
                                           placeholder="[{oxmultilang ident="QUICKORDER_PLACEHOLDER_SEARCHFIELD"}]">
                                </div>
                                <label class="col-sm-6 control-label"></label>
                                <div id="newEntryAutoComplete" style="width:100%;cursor:pointer"></div>
                        </div>
                        <br><br><br><br>
                        <h3>[{oxmultilang ident="BILLING_ADDRESS" }]</h3>

                        [{if $oxcmp_user->oxuser__oxcompany->value != ''}][{$oxcmp_user->oxuser__oxcompany->value}]<br>[{/if}]
                        [{if $oxcmp_user->oxuser__oxfname->value != '' && $oxcmp_user->oxuser__oxlname->value !=''}] [{$oxcmp_user->oxuser__oxfname->value}] [{$oxcmp_user->oxuser__oxlname->value}]<br>[{/if}]
                        [{$oxcmp_user->oxuser__oxstreet->value}] [{$oxcmp_user->oxuser__oxstreetnr->value}]<br>
                        [{if $oxcmp_user->oxuser__oxaddinfo->value != ''}][{$oxcmp_user->oxuser__oxaddinfo->value}]<br>[{/if}]
                        [{$oxcmp_user->oxuser__oxzip->value}] [{$oxcmp_user->oxuser__oxcity->value}]<br>
                        [{$oxcmp_user->oxuser__oxcountry->value}]<br>
                        [{if $oxcmp_user->oxuser__oxfon->value != ''}][{$oxcmp_user->oxuser__oxfon->value}]<br>[{/if}]
                        <br><br><br><br>

                        <h3>[{oxmultilang ident="SHIPPING_ADDRESS" }]</h3>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="iswhitelabel">[{oxmultilang ident="QUICKORDER_WHITE_LABEL" suffix="COLON"}]</label>
                            <div class="col-sm-10">
                                <input class="" id="iswhitelabel" name="iswhitelabel" type="checkbox"
                                       style="margin: 10px 0 0" value="0">
                                [{oxmultilang ident="NEUTRALER_VERSAND_HELP" }]
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="selbstversand">[{oxmultilang ident="VERSANDSELBST" suffix="COLON"}]</label>
                            <div class="col-sm-10">
                                <input type="checkbox" id="selbstversand" name="selbstversand" class="" style="margin: 10px 0 0" onclick="setAdress(
                                        '[{$oxcmp_user->oxuser__oxid->value}]',
                                        '[{$oxcmp_user->oxuser__oxcompany->value}]',
                                        '[{$oxcmp_user->oxuser__oxsal->value}]',
                                        '[{$oxcmp_user->oxuser__oxfname->value}]',
                                        '[{$oxcmp_user->oxuser__oxlname->value}]',
                                        '[{$oxcmp_user->oxuser__oxstreet->value}]',
                                        '[{$oxcmp_user->oxuser__oxstreetnr->value}]',
                                        '[{$oxcmp_user->oxuser__oxaddinfo->value}]',
                                        '[{$oxcmp_user->oxuser__oxzip->value}]',
                                        '[{$oxcmp_user->oxuser__oxcity->value}]',
                                        '[{$oxcmp_user->oxuser__oxcountryid->value}]',
                                        '[{$oxcmp_user->oxuser__oxfon->value}]',
                                        '[{oxmultilang ident="ADRESSEAUSGEFUELLT"}]'
                                        ); if($('#searchadressbook').attr('disabled')){
                                        $('#searchadressbook').removeAttr('disabled');
                                        }else{
                                        $('#searchadressbook').attr('disabled','disabled');
                                        }">[{*oxmultilang ident="FORMFILL"*}]
                            </div>
                        </div>
                        [{oxscript add="
$('#iswhitelabel').change(function(){
							        if($('#iswhitelabel').prop('checked')){
							            $('#upload').css('display', 'block');
										$('#iswhitelabel').val(1);
							        }else{
							            $('#upload').css('display', 'none');
										$('#iswhitelabel').val(0);
							        }
							        /*console.log($('#iswhitelabel').prop('checked'));*/
							        });"}]

                        [{* Adressbuch durchsuchen *}]
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                                <input class="form-control" id="searchadressbook"
                                       placeholder="[{oxmultilang ident="QUICKORDER_SEARCH_CONTACTS"}]">
                                <input type="hidden" id="addressbookid" name="addressbookid">
                                <div id="adressBookAutocomplete"></div>
                            </div>
                        </div>

                        [{* Anrede raus
						<div class="form-group">
							<label class="col-sm-2 control-label">[{oxmultilang ident="TITLE" suffix="COLON"}]</label>
							<div class="col-sm-10">
								<select class="form-control" id="oxsal" name="invadr[oxuser__oxsal]">
									<option value="MR"
									        [{if $value|lower  == "mr"  or $value2|lower == "mr" }]SELECTED[{/if}]>
										[{oxmultilang ident="MR"  }]
									</option>
									<option value="MRS"
									        [{if $value|lower  == "mrs" or $value2|lower == "mrs"}]SELECTED[{/if}]>
										[{oxmultilang ident="MRS" }]
									</option>
								</select>
							</div>
						</div>
						*}]
                        <div class="form-group">
                            <label class="col-sm-2 control-label">[{oxmultilang ident="COMPANY" suffix="COLON" }]</label>
                            <div class="col-sm-10">
                                <input class="form-control" id="oxcompany" name="invadr[oxuser__oxcompany]"
                                       placeholder="[{oxmultilang ident="COMPANY" }]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="firma">[{oxmultilang ident="FIRST_NAME" suffix="COLON" }]</label>
                            <div class="col-sm-10">
                                <input class="form-control" id="oxfname" name="invadr[oxuser__oxfname]"
                                       placeholder="[{oxmultilang ident="FIRST_NAME" }]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">[{oxmultilang ident="LAST_NAME" suffix="COLON" }]</label>
                            <div class="col-sm-10">
                                <input class="form-control" id="oxlname" name="invadr[oxuser__oxlname]"
                                       placeholder="[{oxmultilang ident="LAST_NAME" }]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">[{oxmultilang ident="ADDITIONAL_INFO" suffix="COLON" }]</label>
                            <div class="col-sm-10">
                                <input class="form-control" id="oxaddinfo" name="invadr[oxuser__oxaddinfo]" placeholder="[{oxmultilang ident="ADDITIONAL_INFO" }]">
                            </div>
                        </div>
                        <div class="form-group street">
                            <label class="col-sm-2 control-label">[{oxmultilang ident="STREET_AND_STREETNO" suffix="COLON" }]</label>
                            <div class="col-sm-10">
                                <input class="form-control big" id="oxstreet" name="invadr[oxuser__oxstreet]"
                                       placeholder="[{oxmultilang ident="STREET" }]">
                                <input class="form-control small" id="oxstreetnr" name="invadr[oxuser__oxstreetnr]"
                                       placeholder="[{oxmultilang ident="STREETNO"  }]">
                            </div>
                        </div>
                        <div class="form-group plzcity">
                            <label class="col-sm-2 control-label">[{oxmultilang ident="POSTAL_CODE_AND_CITY" suffix="COLON" }]</label>
                            <div class="col-sm-10">
                                <input class="form-control small" id="oxzip" name="invadr[oxuser__oxzip]"
                                       placeholder="[{oxmultilang ident="POSTAL_CODE"}]">
                                <input class="form-control big" id="oxcity" name="invadr[oxuser__oxcity]"
                                       placeholder="[{oxmultilang ident="CITY"}]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="oxcountryid">[{oxmultilang ident="COUNTRY" suffix="COLON" }]</label>
                            <div class="col-sm-10">
                                <select id="oxcountryid" name="invadr[oxuser__oxcountryid]" id="invCountrySelect"
                                        class="form-control">
                                    <option value="">-</option>
                                    [{assign var="blCountrySelected" value=false}]
                                    [{foreach from=$oViewConf->getCountryList() item=country key=country_id }]
                                    [{assign var="sCountrySelect" value=""}]
                                    [{if !$blCountrySelected}]
                                    [{if (isset($invadr.oxuser__oxcountryid) && $invadr.oxuser__oxcountryid == $country->oxcountry__oxid->value) || (!isset($invadr.oxuser__oxcountryid) && $oxcmp_user->oxuser__oxcountryid->value == $country->oxcountry__oxid->value) }]
                                    [{assign var="blCountrySelected" value=true}]
                                    [{assign var="sCountrySelect" value="selected"}]
                                    [{/if}]
                                    [{/if}]
                                    <option value="[{$country->oxcountry__oxid->value }]" [{$sCountrySelect}]>
                                        [{$country->oxcountry__oxtitle->value }]
                                    </option>
                                    [{/foreach }]
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="oxfon">[{oxmultilang ident="PHONE" suffix="COLON" }]</label>
                            <div class="col-sm-10">
                                <input class="form-control" id="oxfon" name="invadr[oxuser__oxfon]"
                                       placeholder="[{oxmultilang ident="PHONE"}]">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-2 control-label norm"
                                   for="orderRemark">[{oxmultilang ident="MITTEILUNG" suffix="COLON"}]</label>
                            <div class="col-sm-10" style="padding-top:10px;">
								<textarea id="orderRemark" rows="7" name="orderRemark" class="areabox form-control"
                                          aria-describedby="mitteilungsText"></textarea>
                                <span class="help-block"
                                      id="mitteilungsText">[{oxmultilang ident="HERE_YOU_CAN_ENETER_MESSAGE"}]</span>
                            </div>
                            [{*}]
						<label for="orderRemark" class="innerLabel textArea">[{oxmultilang ident="HERE_YOU_CAN_ENETER_MESSAGE"}]</label>
						[{*}]
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">[{oxmultilang ident="DATEI" suffix="COLON" }]</label>
                            <div class="col-sm-10">
                                [{oxmultilang ident="QUICKORDER_PDF_TXT"}]<br><br><br>
                                <input name="lieferschein" type="file" accept=".pdf"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">
                                <button type="button" class="btn btn-default btn-primary pull-right btn-outline-success"
                                        onclick="showConfirmQuickOrder('[{oxmultilang ident="QUICKORDER_COUNT"}]','[{oxmultilang ident="QUICKORDER_ARTICLE"}]','[{oxmultilang ident="QUICKORDER_SINGLE_PRICE"}]','[{oxmultilang ident="BNTOTAL"}]')"><span
                                            class="ladda-label">[{oxmultilang ident="SUBMIT_QUICK_ORDER"}]</span>
                                </button>
                            </div>
                        </div>
                        [{oxifcontent ident="oxrighttocancellegend2" object="oContent"}]
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <span class="help-block">[{$oContent->oxcontents__oxcontent->value}]</span>
                            </div>
                        </div>
                        [{/oxifcontent}]
                </div>
                </form>
                [{/if}]
            </div>
        </div>

        <div id="ordercomplete" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">[{oxmultilang ident="BTN_CLOSE"}]</span></button>
                        <h4 class="modal-title">[{oxmultilang ident="MODAL_ORDER_COMPLETE_TITLE"}]</h4>
                    </div>
                    <div class="modal-body">
                        <p>[{oxmultilang ident="MODAL_ORDER_COMPLETE_BODY"}]</p>

                        [{*block name="checkout_thankyou_main"}]
						[{assign var="order" value=$oView->getOrder()}]
						[{assign var="basket" value=$oView->getBasket()}]

						[{block name="checkout_thankyou_info"}]
						[{oxmultilang ident="THANK_YOU_FOR_ORDER"}] [{$oxcmp_shop->oxshops__oxname->value}]. <br>
						[{oxmultilang ident="REGISTERED_YOUR_ORDER" args=$order->oxorder__oxordernr->value}] <br>
						[{if !$oView->getMailError() }]
						[{oxmultilang ident="MESSAGE_YOU_RECEIVED_ORDER_CONFIRM"}]<br>
						[{else}]<br>
						[{oxmultilang ident="MESSAGE_CONFIRMATION_NOT_SUCCEED"}]<br>
						[{/if}]
					<br>
						[{oxmultilang ident="MESSAGE_WE_WILL_INFORM_YOU" }]<br><br>
						[{/block}]

						[{*block name="checkout_thankyou_proceed"}]
						[{oxmultilang ident="YOU_CAN_GO" }]
						<a id="backToShop" rel="nofollow" href="[{oxgetseourl ident=$oViewConf->getHomeLink()}]" class="link">[{oxmultilang ident="BACK_TO_START_PAGE"}]</a>
						[{if $oxcmp_user->oxuser__oxpassword->value}]
						[{oxmultilang ident="OR"}]
						<a id="orderHistory" rel="nofollow" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_order"}]" class="link">[{oxmultilang ident="CHECK_YOUR_ORDER_HISTORY"}]</a>.
						[{/if}]
						[{/block*}]

                        [{*block name="checkout_thankyou_ts"}]
						[{if $oViewConf->showTs("THANKYOU") && $oViewConf->getTsId() }]
						[{assign var="sTSRatingImg" value="https://www.trustedshops.com/bewertung/widget/img/bewerten_"|cat:$oView->getActiveLangAbbr()|cat:".gif"}]
						<h3 class="blockHead">[{ oxmultilang ident="TRUSTED_SHOPS_CUSTOMER_RATINGS" }]</h3>
						[{oxmultilang ident="RATE_OUR_SHOP"}]
						<div class="etrustTsRatingButton">
							<a href="[{$oViewConf->getTsRatingUrl()}]" target="_blank" title="[{oxmultilang ident="TRUSTED_SHOPS_RATINGS"}]">
								<img src="[{$sTSRatingImg}]" border="0" alt="[{oxmultilang ident="WRITE_REVIEW_2"}]" align="middle">
							</a>
						</div>
						[{/if}]
						[{/block}]
					[{/block*}]
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                onclick="location.href='/index.php?cl=quickorder&fnc=showForm';">
                            [{oxmultilang ident="BTN_OK"}]
                        </button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



    </div>
    [{/capture}]
[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1 }]
<div id="previeworder" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">[{oxmultilang ident="BTN_CLOSE"}]</span></button>
                <h4 class="modal-title">[{oxmultilang ident="MODAL_ORDER_PREVIEW_TITLE"}]</h4>
            </div>
            <div class="modal-body">
                <p>[{oxmultilang ident="MODAL_ORDER_PREVIEW_BODY"}]</p>
            </div>
            <div class="modal-footer">
                <div class="row" style="width:100%">
                    <div class="col-12 col-sm-6">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            [{oxmultilang ident="MODAL_ORDER_BTN_CHANGE"}]
                        </button>
                    </div>
                    <div class="col-12 col-sm-6">
                        <button id="btnPlaceOrder" type="button" class="btn btn-outline-primary ladda-button"
                                data-style="expand-left" onclick="placeQuickOrder('[{oxmultilang ident="QUICKORDER_ERROR_NO_ARTICLE"}]','[{oxmultilang ident="QUICKORDER_ERROR_ADDRESS_FIELD"}]','[{oxmultilang ident="QUICKORDER_ERROR"}]');"><span
                                    class="ladda-label">[{oxmultilang ident="QUICKORDER_BTN_ORDERMANDANTORY"}]</span><span id="spinnerOrder" class="spinner-border spinner-border-sm pull-right" role="status" aria-hidden="true" style="display:none"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->