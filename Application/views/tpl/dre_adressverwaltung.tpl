[{capture append="oxidBlock_content"}]
	<div class="container-fluid">
		[{oxscript include=$oViewConf->getModuleUrl('dre_quickorder','out/src/dre_adressverwaltung.js') priority=1}]

		[{*if $oView->getClassName() eq "adressverwaltung" && !$blHideBreadcrumb}]
			[{include file="widget/breadcrumb.tpl"}]
		[{/if*}]

		[{assign var="userid" value=$oxcmp_user->oxuser__oxid->value}]
		[{*$oxcmp_user->oxuser__oxid->value|var_dump*}]
		[{assign var="adressen" value=$oView->allAdress($userid)}]
		[{*$adressen|var_dump*}]

			<div class="panel panel-default">
				<div class="panel-heading" role="tab">
					<h4 class="pageHead">[{oxmultilang ident="BTN_ADRESSVERWALTUNG"}]</h4>
				</div>


				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-condensed">
							<tr align="left">
								[{*}]
								<th>
									OXID
								</th>
								[{*}]
								<th>
									[{oxmultilang ident="COMPANY"}]
								</th>
								<th>
									[{oxmultilang ident="FIRST_NAME"}]
								</th>
								<th>
									[{oxmultilang ident="LAST_NAME"}]
								</th>
								<th>
									[{oxmultilang ident="STREET_AND_STREETNO"}]
								</th>
								<th>
									[{oxmultilang ident="POSTAL_CODE"}]
								</th>
								<th>
									[{oxmultilang ident="POSTAL_CITY"}]
								</th>
								<th>
									[{oxmultilang ident="COUNTRY"}]
								</th>
								<th>
									[{oxmultilang ident="ACTION"}]
								</th>
							</tr>
							[{foreach from=$adressen item="adresse"}]
								<tr>
									[{*}]
									<td>
										[{$adresse[0]}]
									</td>
									[{*}]
									<td>
										[{$adresse[3]}]
									</td>
									<td>
										[{$adresse[4]}]
									</td>
									<td>
										[{$adresse[5]}]
									</td>
									<td>
										[{$adresse[6]}]  [{$adresse[7]}]
									</td>
									<td>
										[{$adresse[8]}]
									</td>
									<td>
										[{$adresse[9]}]
									</td>
									<td>
										[{$adresse[10]}]
									</td>
									<td>
										<button class="btn btn-danger" onclick="delAdress('[{$adresse[0]}]');"><span class="fa fa-trash"></span></button>
									</td>
								</tr>
							[{/foreach}]
						</table>
					</div>
					[{*$adresse|var_dump*}]
				</div>

				[{*$adresse|var_dump*}]
			</div>
		</div>

[{/capture}]
[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1 }]
