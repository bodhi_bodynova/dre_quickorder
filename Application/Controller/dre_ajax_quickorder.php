<?php

namespace Bender\dre_QuickOrder\Application\Controller;


use OxidEsales\Eshop\Core\Registry;

class dre_ajax_quickorder extends \OxidEsales\Eshop\Application\Controller\FrontendController
{
    protected $_sClass = 'dre_ajax_quickorder';
    // teplate for the view
    protected $_sThisTemplate = 'dre_quick_json.tpl';

    /**
     *
     */
    public function getAdressAutocomplete()
    {
        //
        $oLang = Registry::getLang();
        $arrRet = array();
        //
        $oAddress = oxNew(\OxidEsales\Eshop\Application\Model\Address::class);
        //
        //die(json_encode($_GET['search']));
        $arrResults = $oAddress->filterAdresses($_GET['search']);

        /*
        print_r($arrResults);
        die();
        */
        //
        foreach ($arrResults as $oItem) {
            $arrRet[] = array(
                //
                'oxid' => $oItem->__oxid->rawValue,
                'company' => $oItem->__oxcompany->rawValue ? $oItem->__oxcompany->rawValue : '',
                'sal' => $oItem->__oxsal->rawValue ? $oItem->__oxsal->rawValue : '',
                'fname' => $oItem->__oxfname->rawValue ? $oItem->__oxfname->rawValue : '',
                'lname' => $oItem->__oxlname->rawValue ? $oItem->__oxlname->rawValue : '',
                'street' => $oItem->__oxstreet->rawValue ? $oItem->__oxstreet->rawValue : '',
                'addinfo' => $oItem->__oxaddinfo->rawValue ? $oItem->__oxaddinfo->rawValue : '',
                'streetnr' => $oItem->__oxstreetnr->rawValue ? $oItem->__oxstreetnr->rawValue : '',
                'zip' => $oItem->__oxzip->rawValue ? $oItem->__oxzip->rawValue : '',
                'city' => $oItem->__oxcity->rawValue ? $oItem->__oxcity->rawValue : '',
                'countryid' => $oItem->__oxcountryid->rawValue ? $oItem->__oxcountryid->rawValue : '',
                'fon' => $oItem->__oxfon->rawValue ? $oItem->__oxfon->rawValue : '',
                'message' => $oLang->translateString('ADRESSEAUSGEFUELLT')
                // ende
            );
        }
        //
        die(json_encode($arrRet));
        // ende
    }


}