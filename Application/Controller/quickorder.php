<?php

namespace Bender\dre_QuickOrder\Application\Controller;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Price;
use OxidEsales\Eshop\Core\Field;


class quickorder extends \OxidEsales\Eshop\Application\Controller\FrontendController
{

    protected $_sClass = 'quickorder';
    // teplate for the view
    protected $_sThisTemplate = null;


    public function render()
    {
        parent::render();

        $sPriceSufix = '';
        $oTheme = oxNew(\OxidEsales\Eshop\Core\Theme::class);
        $theme = $this->_sActiveTheme = $oTheme->getActiveThemeId();
        $oUser = $this->getUser();
        $regobject = Registry::getConfig();

        if($oUser == null){
            \OxidEsales\Eshop\Core\Registry::getUtils()->redirect($regobject->getShopHomeURL() . 'cl=account&sourcecl=start');
        }


        if ($oUser->inGroup('oxidpricea')) {
            $sPriceSufix = 'a';
        } elseif ($oUser->inGroup('oxidpriceb')) {
            $sPriceSufix = 'b';
        } elseif ($oUser->inGroup('oxidpricec')) {
            $sPriceSufix = 'c';
        } elseif ($oUser->inGroup('oxidpriced')) {
            $sPriceSufix = 'd';
        } elseif ($oUser->inGroup('oxidpricee')) {
            $sPriceSufix = 'e';
        }



        // get language id
        $iLanguage = Registry::getLang()->getBaseLanguage();

        // set values
        $this->strPriceKey = '__oxprice' . $sPriceSufix;
        $this->strVariantTitleKey = '__oxvarselect' . ($iLanguage ? '_' . $iLanguage : '');
        $this->strArticleTitleKey = '__oxtitle' . ($iLanguage ? '_' . $iLanguage : '');

        /*
        echo '<pre>';
        print_r($this->strPriceKey);
        die();
        */

        if($theme == 'bnsales_wave_child'){
            return 'bn_quickorder_form.tpl';
        } else {
            return 'dre_quickorder_form.tpl';
        }
    }


    public function __construct()
    {

        parent::__construct();
        // load user specific prices


        //print_r($this->strArticleTitleKey);
        // ende
    }



    /**
     *
     */
    public function showForm()
    {
        //
        /**
         * nothing in here ... just show the form
         */
        // ende
    }


    /**
     *
     */
    public function getArticles()
    {
        // searching ..
        $oSearchHandler = oxNew('oxsearch');
        $search = $_GET['artnum'];
        $strSearchString = "SELECT * FROM oxarticles as a WHERE a.OXACTIVE = 1 AND a.OXPARENTID = '' AND (a.oxid IN(SELECT oxid FROM oxarticles WHERE oxtitle LIKE '%" . $search . "%' OR  oxtitle_1 LIKE '%" . $search . "%' OR oxtitle_2 LIKE '%" . $search . "%' OR oxartnum LIKE '%" . $search . "%' OR oxsearchkeys LIKE '%" . $search . "%' OR oxsearchkeys_1 LIKE '%" . $search . "%' OR oxsearchkeys_2 LIKE '%" . $search . "%') OR a.oxid IN(SELECT oxparentid FROM oxarticles WHERE oxtitle LIKE '%" . $search . "%' OR  oxtitle_1 LIKE '%" . $search . "%' OR oxtitle_2 LIKE '%" . $search . "%' OR oxartnum LIKE '%" . $search . "%' OR oxsearchkeys LIKE '%" . $search . "%' OR oxsearchkeys_1 LIKE '%" . $search . "%' OR oxsearchkeys_2 LIKE '%" . $search . "%'))";

        #$strSearchString = $oSearchHandler->getSelectStatement($_GET['artnum']);//.' LIMIT 0, 60	';

        /*$search = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC)->select($strSearchString)->fetchAll();

        print_r($search);
        die();
        */

        // filter results for uniqe oxids
        $arrOXIDs = array();


        $arrSearch = array();
        try {
            $arrSearch = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC)->select($strSearchString)->fetchAll();
        } catch (\Exception $e) {

            $oxEmail = oxNew('oxemail');
            $oxEmail->setSubject('Händlershop FEHLER');
            $oxEmail->setBody("Error in Datei: bn_quickorder.php getArticles()\n" .
                "Eingabe: " . $_GET['artnum'] .
                "Geworfener Fehler: " . $e->getMessage() . "\n SQL: " . $strSearchString);
            $oxEmail->setRecipient('a.bender@bodynova.de', 'André Bender');
            $oxEmail->setReplyTo('server@bodynova.de', 'Server Bodynova');
            $oxEmail->send();
        }


        foreach ($arrSearch AS $arrRecord) {
            $arrOXIDs[] = $arrRecord['OXID'];
        }



        /*
        echo '<pre>';
        print_r($arrOXIDs);
        die();
        */

        //
        $arrOXIDs = array_unique($arrOXIDs);



        // query databse um die oxids zu bekommen
        $strSearchString = 'SELECT ' . $oSearchHandler->getAutosuggestSelectColumns() . ', (SELECT count(oxarticlessub.oxid) AS count FROM oxarticles AS oxarticlessub WHERE oxarticlessub.OXPARENTID=a.OXID AND oxarticlessub.OXACTIVE=1) AS isparent FROM oxarticles AS a WHERE oxid IN (\'' . implode('\',\'', $arrOXIDs) . '\')';

        //$strSearchString = "SELECT * FROM oxarticles as a WHERE a.OXACTIVE = 1 AND a.OXPARENTID = '' AND a.oxid IN ('" . implode('\',\'', $arrOXIDs) . "')'";

        //echo $strSearchString;



        //
        $oArticlesList = oxNew("oxlist");
        $oArticlesList->init("oxbase");
        $oArticlesList->selectString($strSearchString);


        $sPriceSufix = '';
        $oUser = $this->getUser();


        if ($oUser->inGroup('oxidpricea')) {
            $sPriceSufix = 'a';
        } elseif ($oUser->inGroup('oxidpriceb')) {
            $sPriceSufix = 'b';
        } elseif ($oUser->inGroup('oxidpricec')) {
            $sPriceSufix = 'c';
        } elseif ($oUser->inGroup('oxidpriced')) {
            $sPriceSufix = 'd';
        } elseif ($oUser->inGroup('oxidpricee')) {
            $sPriceSufix = 'e';
        }

        // get language id
        $iLanguage = Registry::getLang()->getBaseLanguage();

        // set values
        $this->strPriceKey = '__oxprice' . $sPriceSufix;
        $this->strVariantTitleKey = '__oxvarselect' . ($iLanguage ? '_' . $iLanguage : '');
        $this->strArticleTitleKey = '__oxtitle' . ($iLanguage ? '_' . $iLanguage : '');


        /*
        echo '<pre>';
        echo 'price key<br/>';
        print_r($iLanguage);
        die();

        */



        // speichere die artikel in ein temporäres array
        $arrTmpArticles = array();
        if ($oArticlesList->count()) {
            //
            $oLang = Registry::getLang();
            $oCurrency = $this->getConfig()->getActShopCurrencyObject();
            $sCurrencySign = $oCurrency->sign;
            $sCurrencySide = $oCurrency->side;

            //
            $arrArticles = array();
            //
            foreach ($oArticlesList as $oItem) {

                //
                if ($oItem->__isparent->rawValue > 0) {
                    /*
                    echo '<pre>';
                    echo $oItem->__isparent->rawValue;
                    print_r($oItem);
                    die();
                    */
                    //
                    $oVariantList = oxNew("oxlist");
                    $oVariantList->init("oxbase");
                    //
                    $strQuery = '
                            SELECT
                                OXID,
                                oxartnum,
                                oxvarselect,
                                oxvarselect_1,
                                oxvarselect_2,
                                bnflagbestand,
                                oxvat,
                                oxprice,
                                oxpricea,
                                oxpriceb,
                                oxpricec,
                                oxpriced,
                                oxpricee,
                                oxdelivery
                            FROM
                                oxarticles AS base
                            WHERE
                                    OXPARENTID=\'' . $oItem->__oxid->rawValue . '\'
                                AND
                                    OXACTIVE=1';
                    //LIMIT 10';

                    // query databse um die oxids zu bekommen
                    $oVariantList->selectString($strQuery);

                    /*
                    echo '<pre>';
                    print_r($oVariantList);
                    die();
                    */


                    //die(json_encode(array('articles' => $oVariantList)));
                    foreach ($oVariantList as $oVariantItem) {
                        /*
                        echo '<pre>';
                        print_r($oVariantItem);
                        die();

                        */

                        /*
                        echo '<pre>';
                        print_r($oLang->formatCurrency(Price::getPriceInActCurrency(($oVariantItem->{$this->strPriceKey}->rawValue ? $oVariantItem->{$this->strPriceKey}->rawValue : $oItem->{$this->strPriceKey}->rawValue)), $oCurrency));
                        die();
                        */

                        $strPrice = $oLang->formatCurrency(Price::getPriceInActCurrency(($oVariantItem->{$this->strPriceKey}->rawValue ? $oVariantItem->{$this->strPriceKey}->rawValue : round((($oVariantItem->__oxprice->rawValue / (100 + $oVariantItem->__oxvat->rawValue)) * 100), 2))), $oCurrency);

                        $arrArticles[] = array(
                            'oxid' => $oVariantItem->__oxid->rawValue,
                            'artnum' => $oVariantItem->__oxartnum->rawValue,

                            'title' =>
                                $oItem->__oxtitle->value .
                                ' | ' .
                                $oVariantItem->{$this->strVariantTitleKey}->rawValue,(
                                                    $oItem->{$this->strArticleTitleKey}->rawValue
                                                    ?
                                                    $oItem->{$this->strArticleTitleKey}->rawValue
                                                    :
                                                    $oVariantItem->{$this->strArticleTitleKey}->rawValue
                                                ).
                                                ' | '.
                                                $oVariantItem->{$this->strVariantTitleKey}->rawValue,


                            'price' => $oLang->formatCurrency(Price::getPriceInActCurrency(($oVariantItem->{$this->strPriceKey}->rawValue ? $oVariantItem->{$this->strPriceKey}->rawValue : $oItem->{$this->strPriceKey}->rawValue)), $oCurrency), //$sCurrencySide == 'Front' ? $sCurrencySign . ' ' . $strPrice : $strPrice . ' ' . $sCurrencySign,
                            'bnflagbestand' => $oVariantItem->__bnflagbestand->rawValue,
                            //'oxdelivery' => $oVariantItem->__oxdelivery->rawValue
                            'oxdelivery' => ($oVariantItem->__oxdelivery->rawValue == '0000-00-00' ? '' : date("d.m.y",strtotime($oVariantItem->__oxdelivery->rawValue)))
                        );
                    }
                    /*
                    echo '<pre>';
                    print_r($oItem);
                    die();
                    */
                } else {
                    //
                    $strPrice = $oLang->formatCurrency(Price::getPriceInActCurrency(($oItem->{$this->strPriceKey}->rawValue ? $oItem->{$this->strPriceKey}->rawValue : round((($oItem->__oxprice->rawValue / (100 + $oItem->__oxvat->rawValue)) * 100), 2))), $oCurrency);
                    //
                    $arrArticles[] = array(
                        'oxid' => $oItem->__oxid->rawValue,
                        'artnum' => $oItem->__oxartnum->rawValue,
                        'title' => $oItem->__oxtitle->value,//($oItem->__parenttitle->rawValue ? $oItem->__parenttitle->rawValue : '') . ($oItem->__oxparentid->rawValue ? $oItem->{$this->strArticleTitleKey}->rawValue . ' | ' . $oItem->{$this->strVariantTitleKey}->rawValue : $oItem->{$this->strArticleTitleKey}->rawValue),
                        'price' => $oLang->formatCurrency(Price::getPriceInActCurrency($oItem->{$this->strPriceKey}->rawValue), $oCurrency),  //$sCurrencySide == 'Front' ? $sCurrencySign . ' ' . $strPrice : $strPrice . ' ' . $sCurrencySign,
                        'bnflagbestand' => $oItem->__bnflagbestand->rawValue,
                        //'oxdelivery' => $oItem->__oxdelivery->rawValue
                        'oxdelivery' => ($oItem->__oxdelivery->rawValue == '0000-00-00' ? '' :date("d.m.y",strtotime($oItem->__oxdelivery->rawValue)))

                    );
                    // ende
                }
                if($arrArticles[0]['price'] == '0,00'){
                    $arrArticles[0]['price'] = $oItem->__oxprice->value;
                }
            }
            #print_r($this);
            //
            die(json_encode(array('articles' => $arrArticles)));
            // ende
        } else {
            die(json_encode(array(
                'suggestions' => oxNew('oxsearch')->getFaultCorrectedSearch($_GET['artnum']),
                'suggesttext' => Registry::getLang()->translateString('AUTOSUGGEST_DIDYOUMEAN', Registry::getLang()->getBaseLanguage()),
            )));
        }
        // ende
    }


    /**
     *
     */
    public function submitBasket()
    {
        //
        $oBasket = oxNew('oxbasket');
        $oUser = $this->getUser();

        $sess = Registry::getSession();
        //$sess->initNewSession();


        //mynew
        $oConfig = Registry::getConfig();

        $label = $oConfig->getRequestParameter('iswhitelabel');

        #die($label);
        // additional check if we really really have a user now
        #$oUser = $this->getUser();
        if (!$oUser) {
            return 'user';
        }

        $oUser->oxuser__iswhitelabel = new Field($label, Field::T_RAW);

        $oUser->save();

        // my new end

        // walk through each item and add it
        if (isset($_POST['amount']) && is_array($_POST['amount'])) {
            foreach ($_POST['amount'] AS $sProductID => $dAmount) {
                $res = $oBasket->addToBasket($sProductID, $dAmount);
            }
        } else {
            die(json_encode(array('state' => 0)));
        }
        $oBasket->calculateBasket(true);

        //
        $aMustFields = array('oxfname', 'oxlname', 'oxstreetnr', 'oxstreet', 'oxzip', 'oxcity');
        $boolError = false;
        $arrErrorFields = array();
        foreach ($aMustFields AS $strField) {
            if (!$_POST['invadr']['oxuser__' . $strField]) {
                $boolError = true;
                $arrErrorFields[] = $strField;
            }
        }
        //
        if ($boolError)
            die(json_encode(array('state' => -1, 'fields' => $arrErrorFields)));

        //
        $strDelAdrId = $_POST['addressbookid'];
        $oAddress = oxNew('oxaddress');
        //


        if (!$strDelAdrId || $strDelAdrId == $oUser->getId()) {
            $oAddress->oxaddress__oxuserid = new Field($oUser->getId());


            foreach ($_POST['invadr'] AS $key => $val) {
                $key = str_replace('oxuser', 'oxaddress', $key);
                $oAddress->$key = new Field($val);
            }
            $strDelAdrId = $oAddress->save();
        }



        $remark = $_POST['orderRemark'];
        //


        $sess->setVariable('deladrid', $strDelAdrId);

        //oxSession::setVar( 'deladrid' , $strDelAdrId);
        $oAddress->load($strDelAdrId);
        $_GET['sDeliveryAddressMD5'] = $oUser->getEncodedDeliveryAddress() . $oAddress->getEncodedDeliveryAddress();
        $_GET['deladrid'] = $strDelAdrId;

        //
        $oOrder = oxNew('oxorder');

        #$oOrder->__call('_loadFromBasket' , array($oBasket));
        //oxSession::setVar( 'sess_challenge' , '');
        $sess->setVariable('sess_challenge', '');

        if ($_POST['iswhitelabel'] == 1 || $_POST['iswhitelabel'] == 'on') {
            $sess->setVariable('iswhitelabel', 1);
        }

        // remark:
        $sess->setVariable('ordrem', $remark);


        $iSuccess = $oOrder->finalizeOrder($oBasket, $oUser);


        if (isset($_FILES['lieferschein']['tmp_name'])) {

            $conf = Registry::getConfig();
            $sModulesDir = $conf->getShopMainUrl() . 'modules/bodynova/zzbodynovahaendler/lieferscheine/';


            move_uploaded_file(
                $_FILES['lieferschein']['tmp_name'],
                $sModulesDir . $oOrder->oxorder__oxid->value . substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'))
            );


            // FTP Upload auf fritz.nas
            $ftp_server = '92.50.75.170';
            $ftp_user = 'haendlershop';
            $ftp_pass = 'peanuts30';
            $file = $sModulesDir . $oOrder->oxorder__oxid->value . substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
            $destination_file = 'JetFlash-Transcend16GB-01/FRITZ/salespdf/' . $oOrder->oxorder__oxid->value . substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
            $cid = ftp_connect($ftp_server);
            ftp_login($cid, $ftp_user, $ftp_pass);
            ftp_pasv($cid, true);
            ftp_put($cid, $destination_file, $file, FTP_BINARY);
            ftp_close($cid);

        }

        if ($oOrder->oxorder__oxtransstatus->value == 'OK' && $iSuccess) {
            /*
            if($sess->getVariable('iswhitelabel') == 1){

                $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
                $sQ = 'update oxorder set iswhitelabel=' . $oDb->quote($sess->getVariable('iswhitelabel')) . ' where oxid=' . $oDb->quote($oOrder->getId());
                $oDb->execute($sQ);

                $sess->deleteVariable('iswhitelabel');

            }*/
        }


        die(json_encode(array(
            'state' => $iSuccess,
            'oxorderid' => $oOrder->getId(),
            'ordernr' => $oOrder->oxorder__oxordernr->value,
            'msg' => Registry::getLang()->translateString('REGISTERED_YOUR_ORDER', Registry::getLang()->getBaseLanguage()),
        )));
        // ende
    }



}