<?php

namespace Bender\dre_QuickOrder\Application\Controller;

use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Price;
use OxidEsales\Eshop\Core\Field;


class adressverwaltung extends \OxidEsales\Eshop\Application\Controller\FrontendController
{
    protected $_sClass = 'adressverwaltung';
    // teplate for the view
    protected $_sThisTemplate = null;


    public function render()
    {
        parent::render();
        $oTheme = oxNew(\OxidEsales\Eshop\Core\Theme::class);
        $theme = $this->_sActiveTheme = $oTheme->getActiveThemeId();
        $oUser = $this->getUser();
        if($theme == 'bnsales_wave_child'){
            return 'bn_adressverwaltung.tpl';
        } else {
            return 'dre_adressverwaltung.tpl';
        }
    }

    public function allAdress(){

        $oUser = $this->getUser();
        $regobject = Registry::getConfig();

        if ($oUser == null) {
            \OxidEsales\Eshop\Core\Registry::getUtils()->redirect($regobject->getShopHomeURL() . 'cl=account&sourcecl=start');
        }

        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(\OxidEsales\Eshop\Core\DatabaseProvider::FETCH_MODE_ASSOC);
        //$oDb->setFetchMode(0);
        //$sViewName = getViewName('oxcountry');

        $oUserid = $this->getUser()->getId();

        $sSelect = "
                SELECT *
                FROM oxaddress
                WHERE oxaddress.oxuserid = ?";// . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($oUserid);

        /*
        echo '<pre>';
        print_r($oDb->getAll($sSelect,array($oUserid)));
        die(); */

        return $oDb->getAll($sSelect,array($oUserid));
    }

    public function filterAdresses($strSearch)
    {
        //
        $oAdressList = oxNew("oxlist");
        $oAdressList->init("oxbase");
        //
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        /*
        $sSelect  = "select * from oxaddress ";
        $sSelect .= "where OXUSERID='".$this->getUser()->getId()."' ";
        $sSelect .= "AND MATCH (`OXCOMPANY`, `OXFNAME`, `OXLNAME`, `OXCITY`) AGAINST (".$oDb->quote($strSearch.'*')." IN BOOLEAN MODE)";
        */
        $sSelect = "select a.* from oxaddress as a where a.OXUSERID = '" . $this->getUser()->getId() . "' AND a.OXID IN(SELECT OXID FROM oxaddress WHERE  OXCOMPANY LIKE " . $oDb->quote('%' . $strSearch . '%') . " OR OXFNAME LIKE " . $oDb->quote('%' . $strSearch . '%') . " OR OXLNAME LIKE " . $oDb->quote('%' . $strSearch . '%') . " OR OXADDINFO LIKE " . $oDb->quote('%' . $strSearch . '%') . " OR OXCITY LIKE " . $oDb->quote('%' . $strSearch . '%') . ")";
        //
        //print_r($sSelect);
        //die();
        $oAdressList->selectString($sSelect);
        //
        return $oAdressList;
        // ende
    }

    public function deleteAdress(){
        $oxid = $_GET['oxid'];
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $query = "DELETE FROM oxaddress WHERE OXID = ?";
        $oDb->execute($query,array($oxid));
        $regobject = Registry::getConfig();
        \OxidEsales\Eshop\Core\Registry::getUtils()->redirect($regobject->getShopHomeURL() . 'cl=adressverwaltung');
    }

    public function changeAddress(){

        $country = $_POST['country'];
        list($countryId,$countryTitle) = explode('#',$country);

        $oxid = $_POST['oxid'];
        $company = $_POST['company'];
        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        $street = $_POST['street'];
        $nr = $_POST['nr'];
        $zip = $_POST['zip'];
        $city = $_POST['city'];
        //$country = $_POST['country'];
        $fon = $_POST['fon'];
        $info = $_POST['info'];

        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();

        $updateQuery = 'UPDATE oxaddress SET OXCOUNTRYID=?,OXCOUNTRY=?,OXCOMPANY=?,OXFNAME=?,OXLNAME=?,OXSTREET=?,OXSTREETNR=?,OXZIP=?,OXCITY=?,OXFON=?,OXADDINFO=? WHERE OXID = ?';

        try {
            $oDb->execute($updateQuery, array($countryId,$countryTitle,$company, $fname, $lname, $street, $nr, $zip, $city, $fon, $info, $oxid));
        } catch (DatabaseErrorException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        //die($test);
    }

    public function getCountries($langid = null){

        if($langid == 0){
            $title = "OXTITLE";
        } else if($langid == 1){
            $title = "OXTITLE_1";
        } else if($langid == 2){
            $title = "OXTITLE_2";
        } else {
            $title = "OXID";
        }
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(\OxidEsales\Eshop\Core\DatabaseProvider::FETCH_MODE_ASSOC);
        $query = 'SELECT * FROM oxcountry WHERE OXACTIVE = 1 ORDER BY ' . $title;
        return $oDb->getAll($query);
    }

}