<?php

namespace Bender\dre_QuickOrder\Application\Controller;

use OxidEsales\Eshop\Core\Email;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\EshopCommunity\Internal\Common\Form\FormField;
use OxidEsales\EshopCommunity\Internal\Form\ContactForm\ContactFormBridgeInterface;


class dre_ContactController extends dre_ContactController_parent
{
    /**
     * @return string
     */
    public function render()
    {
        return parent::render();
    }

    /**
     * @return array
     */
    private function getMappedContactFormRequest()
    {
        $request = Registry::getRequest();
        $personData = $request->getRequestEscapedParameter('editval');

        return [
            'email' => $personData['oxuser__oxusername'],
            'firstName' => $personData['oxuser__oxfname'],
            'lastName' => $personData['oxuser__oxlname'],
            'salutation' => $personData['oxuser__oxsal'],
            'subject' => $request->getRequestEscapedParameter('c_subject'),
            'message' => $request->getRequestEscapedParameter('c_message'),
        ];
    }

    /**
     * Composes and sends user written message, returns false if some parameters
     * are missing.
     *
     * @return bool
     */
    public function send()
    {
        $contactFormBridge = $this->getContainer()->get(ContactFormBridgeInterface::class);

        $form = $contactFormBridge->getContactForm();
        $form->handleRequest($this->getMappedContactFormRequest());

        //$array = $this->getMappedContactFormRequest();


        if ($form->isValid()) {
            // mail to owner
            $this->sendContactMail(
                $form->email->getValue(),
                $form->subject->getValue(),
                $contactFormBridge->getContactFormMessage($form)
            );
            // mail to user
            $oxEmail = oxNew(Email::class);

            $oxEmail->setFrom('sales@bodynova.com', 'Sales Bodynova');
            $oxEmail->setSmtp();
            $oxEmail->setSubject($form->subject->getValue());
            $oxEmail->setBody($contactFormBridge->getContactFormMessage($form));
            $oxEmail->setRecipient($form->email->getValue());
            $oxEmail->setReplyTo('sales@bodynova.com', 'Sales Bodynova');
            $oxEmail->send();

        } else {
            foreach ($form->getErrors() as $error) {
                Registry::getUtilsView()->addErrorToDisplay($error);
            }

            return false;
        }
    }

    /**
     * Send a contact mail.
     *
     * @param string $email
     * @param string $subject
     * @param string $message
     */
    private function sendContactMail($email, $subject, $message)
    {
        $mailer = oxNew(Email::class);

        if ($mailer->sendContactMail($email, $subject, $message)) {
            $this->_blContactSendStatus = 1;
        } else {
            Registry::getUtilsView()->addErrorToDisplay('ERROR_MESSAGE_CHECK_EMAIL');
        }
    }

}