<?php

$sLangName  = "Deutsch";

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset'                                                     => 'UTF-8',
    'VERSANDSELBST'                                               => 'An Rechnungsadresse',
    'FORMFILL'                                                    => 'Formular ausfüllen',
    'ADRESSEAUSGEFUELLT'                                          => 'Formular erfolgreich ausgefüllt'
    ];